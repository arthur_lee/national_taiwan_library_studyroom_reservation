import requests
import shutil
import time
from PIL import Image
import pytesseract          # https://github.com/UB-Mannheim/tesseract/wiki
import cv2
import copy
import datetime
import logging

#驗證碼
text = ''
#連線
rs = ''
#驗證碼結果
sendresult = False

#log
LOGGING_FORMAT = '%(asctime)s %(levelname)s: %(message)s'
DATE_FORMAT = '%Y%m%d %H:%M:%S'
logging.basicConfig(level=logging.WARNING, filename='myLog.log', filemode='w', format=LOGGING_FORMAT, datefmt=DATE_FORMAT)


def signin():
    payload = {
        'callCount':1,
        'windowName':'',
        'nextReverseAjaxIndex':0,
        'c0-scriptName':'LoginWebController',
        'c0-methodName':'loadCaptchaImage',
        'c0-id':0,
        'batchId':11,
        'instanceId':0,
        'page':'%2Fsm%2Fhome_web.do',
        'scriptSessionId':'j2ySEeT4Bh6yj5Fe2IGU6**2Pgn/8kt*Pgn-ZuuOB6WS8'
    }
    global rs
    rs = requests.session()
    res = rs.post('http://ireserve.ntl.edu.tw/sm/dwr/call/plaincall/LoginWebController.loadCaptchaImage.dwr', data=payload, stream=True, verify=False)
    print(res.text)
    logging.warning(res.text)



    pg = res.text.index('sm')
    pgend = res.text.index('}')
    pgURL = 'http://ireserve.ntl.edu.tw/'+res.text[pg:pgend-4]
    respg = rs.get(pgURL, stream=True, verify=False)
    with open('ck.png', 'wb') as f:
            respg.raw.decode_content = True
            shutil.copyfileobj(respg.raw, f)
    print(pgURL)
    logging.warning(pgURL)



    def del_noise(img,number):
        height = img.shape[0]
        width = img.shape[1]

        img_new = copy.deepcopy(img)
        for i in range(1, height - 1):
            for j in range(1, width - 1):
                point = [[], [], []]
                count = 0
                point[0].append(img[i - 1][j - 1])
                point[0].append(img[i - 1][j])
                point[0].append(img[i - 1][j + 1])
                point[1].append(img[i][j - 1])
                point[1].append(img[i][j])
                point[1].append(img[i][j + 1])
                point[2].append(img[i + 1][j - 1])
                point[2].append(img[i + 1][j])
                point[2].append(img[i + 1][j + 1])
                for k in range(3):
                    for z in range(3):
                        if point[k][z] == 0:
                            count += 1
                if count <= number:
                    img_new[i, j] = 255
        return img_new

    #pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
    image = cv2.imread('ck.png')

    # 灰度化
    # print(image.shape)
    grayImage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # 二值化
    result = cv2.adaptiveThreshold(grayImage, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 21, 1)
    # 去噪聲
    img = del_noise(result, 3.8)
    img = del_noise(img, 4)
    img = del_noise(img, 3)
    # 加濾波去噪
    im_temp = cv2.bilateralFilter(src=img, d=15, sigmaColor=130, sigmaSpace=150)
    im_temp = im_temp[1:-1,1:-1]
    im_temp = cv2.copyMakeBorder(im_temp, 83, 83, 13, 13, cv2.BORDER_CONSTANT, value=[255])

    global text
    text = pytesseract.image_to_string(img,  config='digits')
    print(text)
    logging.warning(text)


def sendin():
    payload1 = {
        'callCount':1,
        'windowName':'',
        'nextReverseAjaxIndex':0,
        'c0-scriptName':'LoginWebController',
        'c0-methodName':'login',
        'c0-id':0,
        'c0-param0':'string:身分證',
        'c0-param1':'string:密碼',
        'c0-param2':'string:',
        'batchId':12,
        'instanceId':0,
        'page':'%2Fsm%2Fhome_web.do',
        'scriptSessionId':'j2ySEeT4Bh6yj5Fe2IGU6**2Pgn/8kt*Pgn-ZuuOB6WS8'
    }
    payload1['c0-param2'] = payload1['c0-param2'][0:7]+text[0:4]
    res = rs.post('http://ireserve.ntl.edu.tw/sm/dwr/call/plaincall/LoginWebController.login.dwr', data=payload1, stream=True, verify=False)
    if 'result\\\":7' in res.text:
        print('CAPTCHA-ERROR')
        logging.warning('CAPTCHA-ERROR')
        global sendresult
        sendresult = False
    print(res.text)
    logging.warning(res.text)


while sendresult == False:
    sendresult = True
    signin()
    sendin()


#payload2['c0-param2'] 座位3700=>A1 以此類推
payload2 = {
    'callCount':1,
    'windowName':'',
    'nextReverseAjaxIndex':0,
    'c0-scriptName':'ReservationWebController',
    'c0-methodName':'makeReservation',
    'c0-id':0,
    'c0-param0':'number:3740',
    'c0-param1':'string:2020.09.04%2008%3A00%3A01',
    'c0-param2':'string:2020.09.04%2022%3A00%3A00',
    'c0-param3':'boolean:true',
    'c0-param4':'number:0',
    'c0-param5':'null:null',
    'batchId':13,
    'instanceId':0,
    'page':'%2Fsm%2Fhome_web.do',
    'scriptSessionId':'j2ySEeT4Bh6yj5Fe2IGU6**2Pgn/8kt*Pgn-ZuuOB6WS8'
}
t1 = datetime.datetime.now()
t = t1 + datetime.timedelta(days=1)
startTime = datetime.datetime(int(t.strftime('%Y')), int(t.strftime('%m')), int(t.strftime('%d')), 0, 0, 0)
#7天後
x = t + datetime.timedelta(days=7)
payload2['c0-param1'] = payload2['c0-param1'].replace(payload2['c0-param1'][7:17], x.strftime('%Y')+'.'+x.strftime('%m')+'.'+x.strftime('%d'))
payload2['c0-param2'] = payload2['c0-param2'].replace(payload2['c0-param2'][7:17], x.strftime('%Y')+'.'+x.strftime('%m')+'.'+x.strftime('%d'))
while datetime.datetime.now() < startTime:
        time.sleep(0.3)

res = rs.post('http://ireserve.ntl.edu.tw/sm/dwr/call/plaincall/ReservationWebController.makeReservation.dwr', data=payload2, stream=True, verify=False)
print(res.text)
logging.warning(res.text)

#測試延遲1
#time.sleep(1)
#res = rs.post('http://ireserve.ntl.edu.tw/sm/dwr/call/plaincall/ReservationWebController.makeReservation.dwr', data=payload2, stream=True, verify=False)
#print(res.text)

#備位1
payload2['c0-param0'] = payload2['c0-param0'].replace(payload2['c0-param0'][7:11], '3744')
res = rs.post('http://ireserve.ntl.edu.tw/sm/dwr/call/plaincall/ReservationWebController.makeReservation.dwr', data=payload2, stream=True, verify=False)
print(res.text)
logging.warning(res.text)




